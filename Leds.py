# Copyright (c) 2018 Birger Schacht
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTicular purpose ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import RPi.GPIO as GPIO
import time

#BRIGHT = 0 
#MIDDLE = 50 
#LOW = 95
OFF = 100

FAST = 0.01
SLOW = 0.1

class Led():
    channel = None
    pwm = None

    def __init__(self, channel):
        self.channel = channel

    def __enter__(self):
        GPIO.setup(self.channel, GPIO.OUT)
        self.pwm = GPIO.PWM(self.channel, OFF)
        self.pwm.start(OFF)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.pwm.stop()
        GPIO.cleanup(self.channel)
        self.pwm.ChangeDutyCycle(OFF)

    def glow(self):
        for dc in range(100, -1, -5):
            self.pwm.ChangeDutyCycle(dc)
            time.sleep(0.1)
        for dc in range(0, 101, 5):
            self.pwm.ChangeDutyCycle(dc)
            time.sleep(0.1)

