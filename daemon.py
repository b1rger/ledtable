#!/usr/bin/python3
# Copyright (c) 2018 Birger Schacht
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTicular purpose ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from Leds import Led
import random
import RPi.GPIO as GPIO
import time

from multiprocessing.dummy import Pool as ThreadPool 


def main():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    pool = ThreadPool(2) 

    chan_right = {
        'rightred': 3,
        'rightgreen': 5,
        'rightblue': 7,
    }
    chan_left = {
        'leftred': 11,
        'leftgreen': 13,
        'leftblue': 15
    }
    #chan_dict = {**chan_right, **chan_left}
    arr = [chan_right, chan_left]

    while True:
        try:
            pool.map(thread, arr)
        except (KeyboardInterrupt, SystemExit):
            GPIO.cleanup()
            raise

def thread(channels):
    while True:
        channeltupel = random.choice(list(channels.items()))
        glow(channeltupel)
        s = random.randint(1, 200)
        #print("Sleeping {} seconds".format(s/100))
        time.sleep(s/100)

def glow(channeltupel):
    #print("Glowing {}".format(channeltupel[0]))
    with Led(channeltupel[1]) as l:
        l.glow()

if __name__ == "__main__":
    main()
